﻿using System;
using System.Collections.Generic;
using System.Linq;
using Employees.Controllers;
using Employees.DataModel;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Employees.Tests.Controllers
{
    [TestClass]
    public class EmployeesControllerTest
    {
        [TestMethod]
        public void Index()
        {
            EmployeesController controller = new EmployeesController();
            IEnumerable<EmployeeDTO> result = controller.Index();

            Assert.IsNotNull(result);
            EmployeeDTO item1 = result.ElementAt(0);
            Assert.IsNotNull(item1);
            Assert.AreEqual(1, item1.id);
        }

        [TestMethod]
        public void IndexById()
        {
            EmployeesController controller = new EmployeesController();

            EmployeeDTO result1 = controller.Index(1);
            Assert.IsNotNull(result1);
            Assert.AreEqual(1, result1.id);
            Assert.AreEqual("HourlySalaryEmployee", result1.contractTypeName);
            Assert.AreEqual(Convert.ToDecimal(120 * result1.hourlySalary * 12), Convert.ToDecimal(result1.annualSalary));

            EmployeeDTO result2 = controller.Index(2);
            Assert.IsNotNull(result2);
            Assert.AreEqual(2, result2.id);
            Assert.AreEqual("MonthlySalaryEmployee", result2.contractTypeName);
            Assert.AreEqual(Convert.ToDecimal(result2.monthlySalary * 12), Convert.ToDecimal(result2.annualSalary));
        }
    }
}
