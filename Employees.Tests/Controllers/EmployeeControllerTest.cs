﻿using System;
using System.Web.Mvc;
using Employees.Controllers;
using Employees.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Employees.Tests.Controllers
{
    [TestClass]
    public class EmployeeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            EmployeeController controller = new EmployeeController();

            EmployeesViewModel empl = new EmployeesViewModel();
            empl.id = "1";

            ActionResult result = controller.Index(empl);
            Assert.IsNotNull(result);

            var model = ((ViewResultBase)result).Model;
            Assert.IsNotNull(model);

            var employees = ((EmployeesViewModel)model).employees;
            Assert.IsNotNull(employees);
            Assert.AreEqual(1, employees[0].id);
        }

        [TestMethod]
        public void IndexById()
        {
            EmployeeController controller = new EmployeeController();

            EmployeesViewModel empl = new EmployeesViewModel();
            empl.id = "1";

            ActionResult result = controller.Index(empl);
            Assert.IsNotNull(result);

            var model = ((ViewResultBase)result).Model;
            Assert.IsNotNull(model);

            var employees = ((EmployeesViewModel)model).employees;
            Assert.IsNotNull(employees);
            Assert.AreEqual(1, employees[0].id);
            Assert.AreEqual("HourlySalaryEmployee", employees[0].contractTypeName);
            Assert.AreEqual(Convert.ToDecimal(120 * employees[0].hourlySalary * 12), Convert.ToDecimal(employees[0].annualSalary));



            EmployeesViewModel empl2 = new EmployeesViewModel();
            empl2.id = "2";

            ActionResult result2 = controller.Index(empl2);
            Assert.IsNotNull(result2);

            var model2 = ((ViewResultBase)result2).Model;
            Assert.IsNotNull(model2);

            var employees2 = ((EmployeesViewModel)model2).employees;
            Assert.IsNotNull(employees2);
            Assert.AreEqual(2, employees2[0].id);
            Assert.AreEqual("MonthlySalaryEmployee", employees2[0].contractTypeName);
            Assert.AreEqual(Convert.ToDecimal(employees2[0].monthlySalary * 12), Convert.ToDecimal(employees2[0].annualSalary));
        }

        [TestMethod]
        public void Details()
        {
            EmployeeController controller = new EmployeeController();

            EmployeesViewModel empl = new EmployeesViewModel();
            empl.id = "1";

            ActionResult result = controller.Index(empl);
            Assert.IsNotNull(result);

            var model = ((ViewResultBase)result).Model;
            Assert.IsNotNull(model);

            var employees = ((EmployeesViewModel)model).employees;
            Assert.IsNotNull(employees);
            Assert.AreEqual(1, employees[0].id);
        }
    }
}
