﻿using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace Employees.DataAccess
{
    public class MasGlobalTestApi
    {
        /// <summary>
        /// Consumes the API http://masglobaltestapi.azurewebsites.net/swagger/
        /// </summary>
        /// <returns>List of Employees</returns>
        public string GetEmployees()
        {
            string result = string.Empty;
            try
            {
                var url = $"" + ConfigurationManager.AppSettings["UrlApi"] + ConfigurationManager.AppSettings["UriApiGet"];

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = "GET";
                request.ContentType = "application/json";
                request.Accept = "application/json";
                request.Timeout = Convert.ToInt32(ConfigurationManager.AppSettings["UriApiGetTimeOut"]) * 1000;

                using (var response = (HttpWebResponse)request.GetResponse())
                {
                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        return "Error " + response.StatusCode;
                    }

                    using (Stream receiveStream = response.GetResponseStream())
                    {
                        if (receiveStream == null)
                        {
                            return "API did not return response";
                        }

                        using (StreamReader objReader = new StreamReader(receiveStream, Encoding.GetEncoding(response.CharacterSet)))
                        {
                            result = objReader.ReadToEnd();
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                return ex.Message;
            }

            return result;


            /*Employee employee = new Employee();
            employee.id = 1;
            employee.name = "Juan"; ;
            employee.contractTypeName = "HourlySalaryEmployee";
            employee.roleId = 1;
            employee.roleName = "Administrator";
            employee.roleDescription = null;
            employee.hourlySalary = 60000;
            employee.monthlySalary = 80000;

            Employee employee2 = new Employee();
            employee2.id = 2;
            employee2.name = "Sebastian"; ;
            employee2.contractTypeName = "MonthlySalaryEmployee";
            employee2.roleId = 2;
            employee2.roleName = "Contractor";
            employee2.roleDescription = null;
            employee2.hourlySalary = 60000;
            employee2.monthlySalary = 80000;

            return new Employee[] { employee, employee2 };*/
        }
    }
}
