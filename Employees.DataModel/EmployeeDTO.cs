﻿using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Employees.DataModel
{
    [DataContract]
    public class EmployeeDTO : EmployeeBase
    {
        [DataMember]
        [Display(Name = "Annual salary")]
        public decimal annualSalary { get; set; }
    }
}
