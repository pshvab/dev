﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;

namespace Employees.DataModel
{
    [DataContract]
    [Serializable]
    public class EmployeeBase
    {
        [DataMember]
        [Display(Name = "Id")]
        public int id { get; set; }

        [DataMember]
        [Display(Name = "Name")]
        public string name { get; set; }

        [DataMember]
        [Display(Name = "Contract Type")]
        public string contractTypeName { get; set; }

        [DataMember]
        [Display(Name = "Role Id")]
        public int roleId { get; set; }

        [DataMember]
        [Display(Name = "Role Name")]
        public string roleName { get; set; }

        [DataMember]
        [Display(Name = "Role Description")]
        public string roleDescription { get; set; }

        [DataMember]
        [Display(Name = "Hourly Salary")]
        public decimal hourlySalary { get; set; }

        [DataMember]
        [Display(Name = "Monthly Salary")]
        public decimal monthlySalary { get; set; }
    }
}
