﻿using Employees.BusinessLogic;
using Employees.DataModel;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Employees.Controllers
{
    public class EmployeesController : ApiController
    {
        /// <summary>
        /// Recupera todos los Empleados
        /// GET api/<controller>
        /// </summary>
        /// <returns>Lista de los Empleados</returns>
        [HttpGet]
        public IEnumerable<EmployeeDTO> Index()
        {
            return new EmployeeLogic().GetEmployees().ToList();
        }

        /// <summary>
        /// Recupera a un Empleado determinado
        /// GET api/<controller>/5
        /// </summary>
        /// <param name="id">Id de Empleado</param>
        /// <returns>Empleado</returns>
        [HttpGet]
        public EmployeeDTO Index(int id)
        {
            return new EmployeeLogic().GetEmployees().FirstOrDefault(e => e.id == id);
        }
    }
}
