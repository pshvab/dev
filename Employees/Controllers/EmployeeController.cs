﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Employees.BusinessLogic;
using Employees.DataModel;
using Employees.Models;

namespace Employees.Controllers
{
    public class EmployeeController : Controller
    {
        /// <summary>
        /// Recupera todos los Empleados
        /// GET api/<controller>
        /// </summary>
        /// <returns>Lista de los Empleados</returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult Index()
        {
            var model = new EmployeesViewModel();
            model.employees = new EmployeeLogic().GetEmployees().ToList();
            TempData["model"] = null;
            ViewBag.isPostBack = false;
            return View(model);
        }

        /// <summary>
        /// Recupera a un Empleado determinado
        /// GET api/<controller>/5
        /// </summary>
        /// <param name="id">Id de Empleado</param>
        /// <returns>Empleado</returns>
        [HttpPost]
        public ActionResult Index(EmployeesViewModel modelPosted)
        {
            var model = new EmployeesViewModel();
            model.id = (modelPosted.id == null ? string.Empty : modelPosted.id.Trim());

            ViewBag.isPostBack = true;
            ViewBag.id = model.id;

            if (model.id.Length == 0)
            {
                model.employees = new EmployeeLogic().GetEmployees().ToList();
            }
            else
            {
                int id = 0;

                try
                {
                    id = Convert.ToInt32(model.id);
                }
                catch
                {
                    id = 0;
                }

                var employee = new EmployeeLogic().GetEmployees().FirstOrDefault(e => e.id == id);
                List<EmployeeDTO> list = new List<EmployeeDTO>();
                if (employee != null)
                {
                    list.Add(employee);
                }
                model.employees = list;
            }

            TempData["model"] = model;
            return View(model);
        }

        /// <summary>
        /// Recupera a un Empleado determinado
        /// GET api/<controller>/5
        /// </summary>
        /// <param name="id">Id de Empleado</param>
        /// <returns>Empleado</returns>
        [System.Web.Mvc.HttpGet]
        public ActionResult Details(int id)
        {
            return View(new EmployeeLogic().GetEmployees().FirstOrDefault(e => e.id == id));
        }
    }
}