﻿using Employees.DataModel;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Employees.Models
{
    public class EmployeesViewModel : EmployeeDTO
    {
        [Display(Name = "Id:")]
        public string id { get; set; }

        public List<EmployeeDTO> employees { get; set; }
    }
}