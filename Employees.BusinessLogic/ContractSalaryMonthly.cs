﻿namespace Employees.BusinessLogic
{
    class ContractSalaryMonthly : Contract
    {
        public override IContract CalculationMethod()
        {
            return new SalaryMonthly();
        }
    }
}
