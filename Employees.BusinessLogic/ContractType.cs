﻿namespace Employees.BusinessLogic
{
    class ContractType
    {
        public const string HourlySalaryEmployee = "HourlySalaryEmployee";

        public const string MonthlySalaryEmployee = "MonthlySalaryEmployee";
    }
}
