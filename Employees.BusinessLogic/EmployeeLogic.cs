﻿using Employees.DataAccess;
using Employees.DataModel;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Employees.BusinessLogic
{
    public class EmployeeLogic
    {
        /// <summary>
        /// To retrieve the employees information
        /// </summary>
        /// <returns></returns>
        public List<EmployeeDTO> GetEmployees()
        {
            List<EmployeeDTO> resultList = new List<EmployeeDTO>();
            List<EmployeeBase> tempList = null;
            try
            {
                string result = new MasGlobalTestApi().GetEmployees();
                tempList = JsonConvert.DeserializeObject<List<EmployeeBase>>(result);
            }
            catch
            {
                ;
            }

            if (tempList == null)
            {
                return resultList;
            }

            if (tempList.Count == 0)
            {
                return resultList;
            }

            foreach (var item in tempList)
            {
                var empl = new EmployeeDTO();

                empl.id = item.id;
                empl.name = item.name;
                empl.contractTypeName = item.contractTypeName;
                empl.roleId = item.roleId;
                empl.roleName = item.roleName;
                empl.roleDescription = item.roleDescription;
                empl.hourlySalary = item.hourlySalary;
                empl.monthlySalary = item.monthlySalary;
                empl.annualSalary = CalculatedAnnualSalary(item);

                resultList.Add(empl);
            }

            return resultList;
        }

        /// <summary>
        /// Calculate Employee Annual Salary
        /// </summary>
        /// <returns></returns>
        private decimal CalculatedAnnualSalary(EmployeeBase empl)
        {
            if (empl == null)
            {
                return 0.00M;
            }

            switch (empl.contractTypeName)
            {
                case ContractType.HourlySalaryEmployee:
                    return new ContractSalaryHourly().AnnualSalary(empl);
                case ContractType.MonthlySalaryEmployee:
                    return new ContractSalaryMonthly().AnnualSalary(empl);
                default:
                    return 0.00M;
            }
        }
    }
}
