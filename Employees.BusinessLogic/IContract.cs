﻿using Employees.DataModel;

namespace Employees.BusinessLogic
{
    interface IContract
    {
        string Type();

        decimal CalculateAnnualSalary(EmployeeBase empl);
    }
}
