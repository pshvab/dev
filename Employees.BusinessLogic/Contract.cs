﻿using Employees.DataModel;

namespace Employees.BusinessLogic
{
    abstract class Contract
    {
        public abstract IContract CalculationMethod();

        public decimal AnnualSalary(EmployeeBase empl)
        {
            var contract = CalculationMethod();
            return contract.CalculateAnnualSalary(empl);
        }
    }
}
