﻿using Employees.DataModel;

namespace Employees.BusinessLogic
{
    class SalaryMonthly : IContract
    {
        /// <summary>
        /// Type Monthly Salary Employees
        /// </summary>
        /// <returns></returns>
        public string Type()
        {
            return ContractType.MonthlySalaryEmployee;
        }

        /// <summary>
        /// For Monthly Salary Employees the Annual Salary is: MonthtlySalary* 12 
        /// </summary>
        /// <param name="empl">Employee</param>
        /// <returns></returns>
        public decimal CalculateAnnualSalary(EmployeeBase empl)
        {
            return (empl.monthlySalary * 12);
        }
    }
}
