﻿namespace Employees.BusinessLogic
{
    class ContractSalaryHourly : Contract
    {
        public override IContract CalculationMethod()
        {
            return new SalaryHourly();
        }
    }
}
