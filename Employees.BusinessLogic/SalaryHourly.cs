﻿using Employees.DataModel;

namespace Employees.BusinessLogic
{
    class SalaryHourly : IContract
    {
        /// <summary>
        /// Type Hourly Salary Employees
        /// </summary>
        /// <returns></returns>
        public string Type()
        {
            return ContractType.HourlySalaryEmployee;
        }

        /// <summary>
        /// For Hourly Salary Employees the Annual Salary is:  120 * HourlySalary * 12
        /// </summary>
        /// <param name="empl"></param>
        /// <returns></returns>
        public decimal CalculateAnnualSalary(EmployeeBase empl)
        {
            return (120 * empl.hourlySalary * 12);
        }
    }
}
